import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { routes } from './routes-framadate';

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			useHash: true,
			anchorScrolling: 'enabled',
		}),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
