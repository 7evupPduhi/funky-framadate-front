import { Injectable } from '@angular/core';
import ShortUniqueId from 'short-unique-id';

@Injectable({
	providedIn: 'root',
})
export class UuidService {
	private uid = new ShortUniqueId();

	public getUUID(): string {
		return this.uid();
	}
	/**
	 * generate unique id to have a default url for future poll
	 */
	getLongUUID(): string {
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
			const r = (Math.random() * 16) | 0,
				v = c == 'x' ? r : (r & 0x3) | 0x8;
			return v.toString(16);
		});
	}
}
