import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { StorageService } from '../../services/storage.service';
import { ApiService } from '../../services/api.service';
import { ToastService } from '../../services/toast.service';
import { DOCUMENT } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { PollService } from '../../services/poll.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
	public environment = environment;
	public AppTitle = environment.appTitle;
	public nonexistent_email = '';
	public email_sent = false;
	display_poll_search_dialog: any = false;

	constructor(
		@Inject(DOCUMENT) private document: any,
		public storageService: StorageService,
		public toastService: ToastService,
		public titleService: Title,
		private api: ApiService,
		private pollService: PollService,
		private translate: TranslateService,
		private cd: ChangeDetectorRef
	) {}

	ngOnInit() {
		// this.openModalFindPoll();
		this.pollService.step_current = null;
		this.pollService.updateTitle();
	}

	searchMyPolls() {
		const email = this.storageService.vote_stack.owner.email;
		this.email_sent = false;
		this.api.findMyPollsByEmail(email).then(
			(resp) => {
				console.log('resp', resp);

				if (resp) {
					if (resp.data && resp.data.mail_sent == '1') {
						this.toastService.display("C'est bon, vérifiez votre boite mail");
						this.email_sent = true;
					}
				}
			},
			(error) => {
				if (error.response.status == '404') {
					this.toastService.display('Aucun sondage géré par cet email : ' + email);
					this.nonexistent_email = email;
					this.document.querySelector('#search_email').select();
				}
				console.log('error', error);
			}
		);
	}

	openModalFindPoll() {
		this.display_poll_search_dialog = true;
		this.cd.detectChanges();
		let buttonClose = this.document.querySelector('#close_dialog');
		if (buttonClose) {
			buttonClose.focus();
		}
	}

	focusOnCancelButton() {
		this.display_poll_search_dialog = false;
		let buttonClose = this.document.querySelector('#open_popup_button');
		if (buttonClose) {
			buttonClose.focus();
		}
	}
}
