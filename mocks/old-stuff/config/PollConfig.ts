/**
 * une option de date dans les sondages spéciaux
 */
import { environment } from '../../../src/environments/environment';
import { DateChoice, defaultAnswers, otherDefaultDates, PollAnswer } from './defaultConfigs';

export interface DateOption {
	timeList: any;
	literal: string;
	date_object?: object;
}

const baseConfigValues = {
	pollType: 'dates',
	title: '',
	description: '',
	myName: '',
	myEmail: '',
};

/**
 * configuration of the poll, add new fields at will
 */
export class PollConfig {
	menuVisible = true;

	expiracyDateDefaultInDays = 60;
	deletionDateAfterLastModification = 180;
	step = 0; // step in the progress of creating a poll
	stepMax = 3; // step max in the progress of creating a poll
	pollType = 'dates'; // classic or dates

	title: string = environment.production ? '' : 'titre';
	description: string = environment.production ? '' : 'ma description';
	myName: string = environment.production ? '' : 'mon pseudo';
	myComment: string = environment.production ? '' : 'wouah trop bien framadate!';
	isAdmin = !environment.production;
	myVoteStack: any;
	myTempVoteStack = 0;
	myEmail: string = environment.production ? '' : 'tktest@tktest.com';
	myPolls: any = []; // list of retrieved polls from the backend api
	/*
     date specific poll, we have the group_choice to setup different hours (timeSlices) for all possible dates (dateChoices), or use the same hours for all dates
    */
	allowSeveralHours = 'true';
	// access
	visibility = 'link_only'; // visible to anyone with the link:
	voteChoices = 'only_yes'; // possible answers to a vote group_choice: only "yes", "yes, maybe, no"
	creationDate = new Date();
	expirationDate = ''; // expiracy date
	voteStackId = null; // id of the vote stack to update
	pollId = null; // id of the current poll when created. data given by the backend api
	custom_url = null; // id of the current poll when created. data given by the backend api
	currentPoll; // current poll selected with createPoll or getPoll of ConfigService
	passwordAccess = 0;
	password = '';
	customUrl = ''; // custom custom_url in the url, must be unique
	customUrlIsUnique = null; // given by the backend
	urlSlugPublic = null;
	urlPublic = environment.production ? '' : document.location.href + '/poll/id/4';
	urlAdmin = document.location.href + '/admin/d65es45fd45sdf45sd345f312sdf31sgfd345';
	adminKey = ''; // key to change config of the poll
	owner_modifier_token = ''; // key to change a vote stack
	canModifyAnswers = true; // bool for the frontend selector
	whoCanChangeAnswers = 'everybody'; // everybody, self, nobody (= just admin)
	dateList: any = otherDefaultDates; // sets of dateChoices as strings, config to set identical time for dateChoices in a special dateChoices poll
	timeList: DateChoice[] = otherDefaultDates; // ranges of time expressed as strings

	answers: PollAnswer[] = defaultAnswers;
	// modals
	displayConfirmVoteModalAdmin = false;

	resetConfig() {
		const self = this;
		Object.keys(baseConfigValues).forEach((key) => {
			self[key] = baseConfigValues[key];
		});
	}
}
